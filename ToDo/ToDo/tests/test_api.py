from django.test import TestCase
from rest_framework.test import APIClient, RequestsClient
from ..models.models import TaskList, ListAccess, Task
from django.contrib.auth.models import User
import base64
import json

class APIAuthTest(TestCase):
    """ Test module for Authentication """

    def setUp(self):
        self.client = APIClient()
        self.requests_client = RequestsClient()
        self.valid_user = {
            'username': 'user_1',
            'password': 'password',
            'email': 'user_1@email.com',
            'firstname': 'firstname',
            'lastname': 'lastname',
        }

    def test_register_user_and_login(self):
        response = self.client.post('/login/register', self.valid_user, format='json')
        self.assertEqual(response.data['status'], "Success")
        user = User.objects.get(username=self.valid_user['username'])
        self.assertEqual(user.email, self.valid_user['email'])

        encoded_credentials = base64.b64encode(bytearray(self.valid_user['username'] + ':' + self.valid_user['password'], 'utf8'))
        self.requests_client.headers.update({'Authorization': 'Basic ' + encoded_credentials.decode() })
        response = self.requests_client.post('http://testserver/login/')
        json_response = json.loads(response.text)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access_token' in json_response)

class APIListTest(TestCase):
    """ Test module for Lists """

    def setUp(self):
        self.client = APIClient()
        self.requests_client = RequestsClient()
        self.valid_user = {
            'username': 'user_1',
            'password': 'password',
            'email': 'user_1@email.com',
            'firstname': 'firstname',
            'lastname': 'lastname',
        }
        # self.user = User.objects.create(self.valid_user)
        self.user = User.objects.create_user(self.valid_user['username'], self.valid_user['email'], self.valid_user['password'])
        self.valid_list_params = {
            'name': 'list_name',
            'description': 'list_description',
        }
        self.valid_task_params = {
            'list_id': 1,
            'name': 'list_name',
            'description': 'list_description',
        }

    def test_list_creation(self):
        # do login
        encoded_credentials = base64.b64encode(bytearray(self.valid_user['username'] + ':' + self.valid_user['password'], 'utf8'))
        self.requests_client.headers.update({'Authorization': 'Basic ' + encoded_credentials.decode() })
        login_response = self.requests_client.post('http://testserver/login')
        json_response = json.loads(login_response.text)
        access_token = json_response['access_token']
        self.assertEqual(login_response.status_code, 200)
        self.assertTrue('access_token' in json_response)

        # do post to add list
        self.requests_client.headers.update({'AUTHORIZATION': 'Bearer ' + access_token })
        create_response = self.requests_client.post('http://testserver/lists/add', data= self.valid_list_params)
        json_response = json.loads(create_response.text)
        list_id = json_response['data']['id']
        self.assertEqual(create_response.status_code, 201)
        self.assertEqual(json_response['status'], 'success')

        # do post to add task
        self.requests_client.headers.update({'AUTHORIZATION': 'Bearer ' + access_token })
        create_response = self.requests_client.post('http://testserver/tasks/add', data= self.valid_task_params)
        json_response = json.loads(create_response.text)
        self.assertEqual(create_response.status_code, 201)
        self.assertEqual(json_response['status'], 'success')
