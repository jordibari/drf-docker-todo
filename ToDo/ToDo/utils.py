from datetime import datetime, timedelta
from django.contrib.auth.models import User
import jwt
from django.conf import settings

@staticmethod
def validate_required_input(param, value):
    """
    Function to validate the required input of post method

    :param param: It can take one of the values from required param of post method
    :param value: Value of the passed param
    :return: value if value passes the validation criteria for the given param
    :raises: ValidationError: if value doesn't pass the validation criteria for the given param
    """

    if param == 'username':
        if value is not None and type(value) == str and len(value) > 0:
            if User.objects.filter(username=value).exists():
                raise ValidationError('Username already taken, please try with a different username')
            return value
        else:
            raise ValidationError('Invalid username, it can\'t be empty')

    elif param == 'password':
        if value is not None and type(value) == str and len(value) >= 8:
            return value
        else:
            raise ValidationError('Invalid Password, password should be at least 8 characters long')

    elif param == 'email':
        if value is not None and type(value) == str and len(value) > 0:
            try:
                validate_email(value)
            except ValidationError:
                raise ValidationError('Invalid Email')
            else:
                if User.objects.filter(email=value).exists():
                    raise ValidationError('E-mail already in use, please try logging in instead')
                return value
        else:
            raise ValidationError('Invalid Email')

    else:
        raise ValidationError('Invalid Input Param Passed')

def generate_tokens(user):

    if not (isinstance(user, User)):
        return None

    utc_now = datetime.utcnow()
    access_payload = {
        'iat': utc_now,
        'exp': utc_now + timedelta(minutes=60),
        'nbf': utc_now,
        'iss': "http://localhost:8000/login",  # this has to be replaced with application domain after deploying
        'username': user.username,
        'email': user.email,
        'type': 'access'
    }

    access_token = jwt.encode(access_payload, settings.SECRET_KEY, algorithm='HS256')

    refresh_payload = {
        'iat': utc_now,
        'exp': utc_now + timedelta(days=2),
        'nbf': utc_now,
        'iss': "http://localhost:8000/login",  # this has to be replaced with application domain after deploying
        'username': user.username,
        'type': 'refresh',
    }

    refresh_token = jwt.encode(refresh_payload, settings.SECRET_KEY, algorithm='HS256')

    return access_token, refresh_token
