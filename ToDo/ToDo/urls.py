"""ToDo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from .views import views, auth_views
from rest_framework.routers import SimpleRouter
from .views.mor_views import (CoordinateViewSet, PlaceViewSet, MeasurementEquipmentConfigurationViewSet,
FTPServerViewSet, MeasurementEquipmentViewSet, MeasurementResultViewSet, InversionConfigurationViewSet,
InversionResultViewSet, ImageConfigurationViewSet, ImageResultViewSet)

router = SimpleRouter()
router.register('coords', CoordinateViewSet, basename='Coordinate')
router.register('places', PlaceViewSet, basename='Place')
router.register('m_e_configs', MeasurementEquipmentConfigurationViewSet, basename='MeasurementEquipmentConfiguration')
router.register('ftp_servers', FTPServerViewSet, basename='FTPServer')
router.register('m_equips', MeasurementEquipmentViewSet, basename='MeasurementEquipment')
router.register('m_results', MeasurementResultViewSet, basename='MeasurementResult')
router.register('inv_configs', InversionConfigurationViewSet, basename='InversionConfiguration')
router.register('inv_results', InversionResultViewSet, basename='InversionResult')
router.register('img_configs', ImageConfigurationViewSet, basename='ImageConfiguration')
router.register('img_results', ImageResultViewSet, basename='ImageResult')
urlpatterns = router.urls

urlpatterns += [
    path('admin/', admin.site.urls),
    # path('hello_world', views.HelloWorld.as_view()),
    # Paths for login
    re_path(r'^login(?:\/)?$', auth_views.Login.as_view()),
    path('login/register', auth_views.Register.as_view()),
    # Paths for lists and tasks
    # path('lists/add', views.ListAdd.as_view()),
    # path('lists/list', views.ListFetch.as_view()),
    # path('tasks/add', views.TaskAdd.as_view()),
    # path('tasks/list', views.TaskFetch.as_view()),
]
